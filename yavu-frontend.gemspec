# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'yavu/frontend/version'

Gem::Specification.new do |spec|
  spec.name          = "yavu-frontend"
  spec.version       = Yavu::Frontend::VERSION
  spec.authors       = ["Christian Rodriguez"]
  spec.email         = ["car@cespi.unlp.edu.ar"]
  spec.description   = %q{Main rendering feature and basic server}
  spec.summary       = ""
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.metadata['allowed_push_host'] = "http://gems.desarrollo.unlp.edu.ar" if spec.respond_to?(:metadata)

  spec.add_development_dependency "bundler", "~> 1.10"
  spec.add_development_dependency "cespigems", "~> 0.1"
  spec.add_development_dependency "rake"

  spec.add_runtime_dependency "yavu-api-resource" ,     "~> 1.2"
  spec.add_runtime_dependency "sprockets-helpers",      "~> 1.1.0"
  spec.add_runtime_dependency "yui-compressor",         "~> 0.11.0"
  spec.add_runtime_dependency "erubis-auto",            "~> 1.0.1"
  spec.add_runtime_dependency "uglifier",               "~> 2.2.1"
  spec.add_runtime_dependency "sass",                   "~> 3.2.19"
  spec.add_runtime_dependency "sinatra",                "~> 1.4.5"
  spec.add_runtime_dependency "rack_csrf"
  spec.add_runtime_dependency "rest-client",            "~> 1.6.7"
  spec.add_runtime_dependency "rest-client-components"
  spec.add_runtime_dependency "redis",                  "~> 3.2.1"
  spec.add_runtime_dependency "redis-namespace",        "~> 1.5.2"
  spec.add_runtime_dependency "sidekiq",                "~> 3.4.2"
  spec.add_runtime_dependency "sidekiq-cron",           "~> 0.3.0"
  spec.add_runtime_dependency "pidfile",                "~> 0.3.0"
  spec.add_runtime_dependency "htmlentities",           "~> 4.3.2"
  spec.add_runtime_dependency "actionpack",             "~> 4.0.10"
  # Fix a dependency problem of sidekiq-cron (is too open)
  spec.add_runtime_dependency "rufus-scheduler",        "= 3.2.1"
end
