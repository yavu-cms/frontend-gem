require 'uri'
require 'rack/csrf'
require 'htmlentities'

module Yavu
module Frontend
  module Helpers
    module Common
      def self.included(base)
        base.extend(ClassMethods)
      end

      module ClassMethods
        attr_accessor :base_url, :assets_configuration, :media_url, :service_prefix,
                      :sprockets_environment, :logo, :routes, :runtime_options

        def configure(&block)
          yield self
          raise 'service_prefix must be set' unless service_prefix
          raise 'base_url must be set' unless base_url
          if assets_configuration
            assets_configuration[:base_url] = base_url
            assets_configuration[:service_prefix] = service_prefix
          end
          configure_sprockets
          self.runtime_options ||= Hash.new
          self.runtime_options.stringify_keys!
        end

        def configure_sprockets
          sprockets_environment = Yavu::Frontend::Util::AssetConfigurationFactory.sprockets_environment assets_configuration
          Yavu::Frontend::Util::AssetConfigurationFactory.configure_sprockets_helpers assets_configuration, sprockets_environment
        end
      end

      TRANSPARENT_PX_IMAGE = 'data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=='.freeze
      LOADER_GIF_IMAGE = '<img src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==" class="srv-loader">'.freeze
      DEFAULT_IMAGE_SIZE = 'medium'.freeze

      def request
        server && server.request
      end

      def session
        request && request.session || {}
      end

      def env
        request && request.env || {}
      end

      def runtime_option(key, default = nil)
        self.class.runtime_options.has_key?(key.to_s) ? self.class.runtime_options[key.to_s] : default
      end

      def t(key, args = {})
        key_segments = key.split('.')
        default_translation = args[:default] || key_segments.last.to_s.humanize
        component_name = key_segments.shift
        translation_for_component = translations[component_name][locale] || {}
        translated_value = key_segments.reduce(translation_for_component) { |stream, i| stream[i] || break }
        translated_value = translated_value.gsub(/#\{(\w+)\}/) { args[$1.to_sym] || "\{#{$1}\?\}" } unless args.empty? or translated_value.nil?
        translated_value || default_translation
      rescue Exception
        key
      end

      def h(string)
        HTMLEntities.new.encode(string, :decimal).html_safe
      end

      def raw(string)
        string.html_safe if string
      end

      def root_path
        base_url
      end

      def link_to(title, dst, html_options = {}, options = {})
        if dst.respond_to? :open_in_new_tab
          html_options.merge!({ target: '_blank' }) if dst.open_in_new_tab
        end
        "<a href=\"#{url_for(dst, options)}\" #{html_attributes html_options}>#{title}</a>".html_safe
      end

      def logo
        self.class.logo
      end

      def base_url
        self.class.base_url
      end

      def url_for(object, options = {})
        return object                           if object.instance_of? String
        return medium_url(object, options)      if object.is_a? Yavu::API::Resource::Medium
        return object.redirect_to_external_link if object.respond_to?(:redirect_to_external_link) && object.redirect_to_external_link.present?
        if (route = route_for(object))
          format = options[:format].present? ? ".#{options[:format]}" : ''
          normalize_uri("#{route}#{format}", options[:absolute].present?)
        else
          '#'
        end
      end

      def normalize_uri(uri, make_absolute = false)
        (make_absolute && uri.to_s !~ %r{\A(https?:)?//} ? root_path : '') + uri.to_s
      end

      def medium_url(medium, options)
        case medium.type
        when :audio
          "TODO: Audio => #{medium.url}"
        when :embedded
          embedded_url(medium, options)
        when :image
          image_url(medium, options)
        when :file
          file_url(medium, options)
        end
      end

      #TODO: options and html_options parameters do not used for now
      def embedded_tag(medium, options = {}, html_options = {})
        medium = medium.medium if medium.is_a? Yavu::API::Resource::ArticleMedium
        medium.content.html_safe
      end

      #TODO: Caption is no used
      def audio_tag(medium, options = {}, html_options = {})
        medium = medium.medium if medium.is_a? Yavu::API::Resource::ArticleMedium
        "<audio controls><source src=\"#{file_url(medium, options)}\"></audio>".html_safe
      end

      def medium_tag(medium, options = {}, html_options = {})
        case medium.type.to_sym
        when :audio
          audio_tag(medium, options, html_options)
        when :embedded
          embedded_tag(medium, options, html_options)
        when :image
          image_tag(medium, options, html_options)
        when :file
          file_tag(medium, options, html_options)
        else
          "<!-- MEDIUM TAG NOT FOUND: #{medium.type} -->".html_safe
        end
      end

      %i{ xsmall small medium big xbig }.each do |size|
        send :define_method, :"#{size}_image_tag" do |medium, html_options = {}|
          image_tag(medium, {size: size}, html_options)
        end
      end

      # @feature_toggle 'lazy_loading' (default: true) to enable/disable lazy loading of images
      def image_tag(medium, options = {}, html_options = {})
        # Get ArticleMedium title and alt tags if present
        # At this point, medium can be a pure Medium or an ArticleMedium
        auto_info = %i(alt title caption name credits date).lazy.map { |attr| medium.try(attr) }.detect { |i| i.present? }
        # Resolves the identity of medium. By default, it assumes medium is a Medium
        medium = medium.medium if medium.is_a? Yavu::API::Resource::ArticleMedium
        html_options.reverse_merge! src: image_url(medium, options), title: (auto_info), alt: (auto_info)
        tag = "<img #{html_attributes html_options} />"
        if options[:lazy] && runtime_option('lazy_loading', true)
          lazy_loaded_image_tag(tag, html_options, size: options[:size])
        else
          tag.html_safe
        end
      end

      def file_tag(medium, options = {}, html_options = {})
        link_to medium.caption, medium.medium, html_options, options
      end

      # You may want to note that using lazy-loaded images requires https://github.com/vvo/lazyload present on the page
      def lazy_loaded_image_tag(regular_image_tag, html_options, placeholder_src: nil, size: nil, force_sizes: false)
        options = html_options.dup
        options[:'data-src'] = options[:src]
        options[:src] = placeholder_src || TRANSPARENT_PX_IMAGE
        options[:onload] = 'lzld(this)'
        if force_sizes
          options[:width] ||= medium_dimension(medium, size, 'width')
          options[:height] ||= medium_dimension(medium, size, 'height')
        end
        <<-HTML.html_safe
        <noscript>
          #{regular_image_tag}
        </noscript>
        <img #{html_attributes options} />
        HTML
      end

      def medium_dimension(medium, version, dimension)
        version ||= DEFAULT_IMAGE_SIZE
        medium.send("#{version}_dimensions")[dimension]
      end

      def youtube_image(content, quality = :standard)
        uri = URI.parse(content)
        id = uri.query.split('=')[1]
        frame = {
          medium: "http://img.youtube.com/vi/:youtube_id/mqdefault.jpg",
          standard: "http://img.youtube.com/vi/:youtube_id/0.jpg",
          hi: "http://img.youtube.com/vi/:youtube_id/hqdefault.jpg",
          max: "http://img.youtube.com/vi/:youtube_id/maxresdefault.jpg"
        }[quality ].gsub(':youtube_id', id)
      end

      def jw_video_tag(medium)
        medium.content.html_safe if medium.is_jw
      end

      def embedded_url(medium, options = {})
        # The content is the url to embedded media
        # TODO: Expand this definition to provide more video services

        medium.content
      end

      def image_url(medium, options = {})
        return medium if medium.is_a? String
        size = options[:size] || DEFAULT_IMAGE_SIZE
        normalize_uri([self.class.media_url, (medium.send("#{size}_url") || medium.url)].join, options[:absolute].present?)
      end

      def file_url(medium, options = {})
        normalize_uri([self.class.media_url, medium.url].join, options[:absolute].present?)
      end

      def article_date(article, date_format = :short, time_format = :short)
        parts = {
          date: I18n.l(article.date.to_date, format: date_format),
          time: I18n.l(article.time.to_time, format: time_format)
        }
        today = (parts[:date] == I18n.l(Date.today, format: date_format)) ? 'today' : 'past'
        <<-EOT.html_safe
          <span class="article-date #{today}">#{parts[:date]}</span>
          <span class="article-time"> | #{parts[:time]}</span>
        EOT
      end

      # TODO: Remove this, is deprecated
      def accounting_tag
      end

      def html_attributes(hash)
        hash.map { |k, v| "#{k}='#{v}'" }.join(' ')
      end

      def route_for(object, route_name: nil)
        return service_path(object) if object.is_a?(Yavu::API::Resource::ComponentService)
        route = self.class.routes.detect { |r| r.name == route_name } if route_name
        (route || most_suitable_route_for(object)).for(object)
      end

      def most_suitable_route_for(object)
        self.class.routes.detect { |route| route.is_default_for?(object) } ||
          self.class.routes.detect { |route| route.can_process?(object) } ||
            ::Yavu::API::Resource::NullRoute.new # Default to a null route which does nothing
      end

      def service_path(object)
        "#{self.class.service_prefix}/#{object.component_configuration_id}/#{object.id}"
      end

      def search_path(**args)
        route_name = args.delete(:route_name).presence
        route_for(Yavu::API::Resource::SearchQuery.new(**args), route_name: route_name)
      end

      def service_container(service, params = {}, loading_text = LOADER_GIF_IMAGE)
        return unless service
        params[:id] ||= "#{service.id}-#{Time.now.nsec}"
        parameters = params.delete(:params) || {}
        <<-EOT.html_safe
<span id="#{params[:id]}" #{html_attributes(params)}>#{loading_text}</span>
<script>
  if (typeof $ === 'undefined') {
    console.error('service_container helper needs jQuery or compatible to perform asynchronous requests.');
  } else {
    $(function($) {
      $.ajax({
        url: '#{url_for service}', type: '#{service.http_method.upcase}', data: #{parameters.to_json},
        success: function(data) { $('##{params[:id]}').html(data); },
        error: function() { $('##{params[:id]}').empty(); }
      });
    });
  }
</script>
        EOT
      end

      def csrf_hidden_tag
        Rack::Csrf::tag(env).html_safe
      end
    end
  end
end
end
