Gem.find_files('yavu/frontend/ext/yavu/api/resource/model/**/*rb').each { |x| require x }
require 'yavu/frontend/renderer/internal_server_error'
require 'erubis/auto'

module Yavu
module Frontend
module Renderer
  class Base
    include Sprockets::Helpers
    include Yavu::Frontend::Helpers::Common

    attr_accessor :logger, :error_template, :layouts, :partials, :translations, :seo, :image_link, :locale, :server, :formats

    # Creates a renderer with:
    #  * A route that provides all the information needed by the renderer
    #  * A logger to log errors or debug information
    def initialize(route_resource, server: nil, locale: nil, logger: nil)
      self.server = server
      self.logger = logger || Logger.new(STDOUT)
      self.error_template = route_resource.error || ''
      self.layouts = route_resource.layouts || {}
      self.partials = route_resource.partials || {}
      self.translations = route_resource.translations || {}
      self.locale = locale || I18n.locale || 'en'
      self.formats = route_resource.formats || ['text/html']
      I18n.locale = self.locale
      self.logger.debug('Frontend') { "Created #{self.class} formats=#{formats.join(',')} partials=#{log_values(partials.keys)}" }
    end

    # Helper to render
    def render_component(template_name, component_context)
      if component_context.nil?
        logger.debug('Fronted') { "Trying to render missing component context (probably misconfigured frontend) template=#{template_name}" }
        raise Yavu::Frontend::Server::MisconfiguredServerError.new("We're gonna restart frontend server due a possible misconfigured error on it")
      elsif component_context.errors?
        error "Error in Component#context or Component#extras error=#{component_context.errors.join ","}"
      else
        logger.debug('Frontend') { "Render component class=#{self.class}# template=#{template_name} locals=#{log_values(component_context._locals)}" }
        render(partial: template_name, locals: component_context._locals)
      end
    end

    def render_cover_article(cover_article, locals = {})
      render partial: cover_article.partial, locals: { article: cover_article.article }.merge(locals)
    end

    def render(options, show_trace = true)
      raise 'render must be called with a hash of options' unless options.is_a?Hash
      raise 'render must specify partial: as option' unless options[:partial]
      template_name = options[:partial]
      if partials[template_name]
        locals = options.delete(:locals) || {}
        logger.debug('Frontend') { "Render template class=#{self.class} method=#{__method__} template=#{template_name} locals=#{log_values(locals)}" }
        _render_data(partials[template_name], options: options, locals: locals)
      else
        error "Unknown template: #{template_name.inspect}"
      end
    rescue InternalServerError => e
      raise
    rescue Exception => e
      show_bt = ->(bt) { show_trace ? bt.first(3).join("\n") : 'N/A' }
      error "template=#{template_name} error=#{e.message} trace=#{show_bt.(e.backtrace)}", __method__
    end

    def render_seo
      <<-EOT.html_safe
        <title>#{h(seo[:title])}</title>
        <meta name="description" content="#{h(seo[:description])}">
        <meta name="keywords" content="#{h(seo[:keywords])}">
      EOT
    end

    def render_image_link
      <<-EOT.html_safe
        <link rel="image_src" href="#{image_link}" />
      EOT
    end

    # This method is where it all begins. A Renderer::Base object must call output to render a page
    def output(locals, format = default_format)
      set_additional_information_with locals
      _render_data(layout(format), locals: locals)
    rescue Yavu::Frontend::Server::MisconfiguredServerError, InternalServerError => e
      raise
    rescue Exception => e
      error "Output error: #{e.message}", __method__, e.backtrace
    end

    def layout(format)
      layouts[format] || layouts[default_format]
    end

    def default_format
      'text/html'
    end

    def raw_output(data, options: {}, locals: {})
      _render_data(data, options: options, locals: locals)
    rescue Exception => e
      error "Output error: #{e.message}", __method__, e.backtrace
    end

    protected

    # Hook method to set any additional information to the locals hash
    def set_additional_information_with(locals)
      self.seo        = locals[:seo] rescue { title: '', description: '', keywords: '' }
      self.image_link = begin
        if locals[:article] && locals[:article][:main_image]
          locals[:article][:main_image][:medium] && url_for(locals[:article][:main_image][:medium], absolute: true)
        elsif logo
          url_for asset_path(logo.file), absolute: true
        end
      rescue
        ''
      end
    end

    def _render_data(data, options:{}, locals: {})
      scope = options[:scope] || self
      template = compile_template(data, options)
      template.render(scope, locals).html_safe
    end

    def error(message, method = nil, trace = nil)
      logger.error('Frontend') do
        error_method = method || caller_locations(1, 1)[0].label
        "Error in class=#{self.class.name} method=#{error_method} message=#{message} trace=#{trace.try(:join, "\n") || 'N/A'}"
      end
      _render_data(error_template, locals: {error: message})
    rescue
      ''
    end

    def compile_template(data, options)
      Tilt::ErubisTemplate.new(nil, 1, options.merge(engine_class: Erubis::Auto::EscapedEruby)) { data }
    end

    def log_values(locals)
      locals.map { |k, v| "#{k}:#{v.inspect.truncate(40)}" }.join ','
    end
  end
end
end
end
