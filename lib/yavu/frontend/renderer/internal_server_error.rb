module Yavu
  module Frontend
    module Renderer
      # Exception class that is specifically intended to be raised when
      # an irrecoverable error is encountered. This kind of errors will
      # be propagated up through the rendering chain.
      class InternalServerError < RuntimeError
      end
    end
  end
end