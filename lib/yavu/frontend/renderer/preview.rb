module Yavu
module Frontend
module Renderer

  class Preview < Yavu::Frontend::Renderer::Base
    def url_for_with_preview(object, options = {})
      return url_for_without_preview(object, options) if object.is_a?(Yavu::API::Resource::ComponentService)
      "javascript:console.log('#{url_for_without_preview(object, options)}');true"
    end

    alias_method_chain :url_for, :preview

    # TODO: Remove this. is deprecated
    def accounting_tag
    end
  end

end
end
end
