module Rack
  module Mime
    MIME_EXTENSIONS = MIME_TYPES.invert

    def mime_extension(type, fallback='.a')
      MIME_EXTENSIONS.fetch(type.to_s.downcase, fallback)
    end
    module_function :mime_extension
  end
end
