module Yavu::API::Resource
  class RedirectRoute < Route
    def controller
      route = self
      -> do
          uri = URI(route.redirect_url)
          extras = params[:splat].try(:join)
          uri.path += "/#{extras}" unless extras.blank?
          uri.query = request.query_string unless request.query_string.blank?
          redirect uri.to_s
      end
    end
  end
end
