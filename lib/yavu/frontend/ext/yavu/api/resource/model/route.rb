require "yavu/frontend/ext/util/mime"

module Yavu::API::Resource
  class Route < Base
    def http_method
      :get
    end

    def is_default_for?(object)
      can_process?(object) && default_route
    end

    def for(object)
      replace_placeholders_with(object) if can_process?(object)
    end

    def can_process?(object)
      false
    end

    def controller(status_code: 200)
      route = self
      -> do
        begin
          context = fetch_context(route)
          cache_control *route.cache_control  if route.cache_control.present?
          etag context.etag                   if context.etag.present?
          last_modified context.last_modified if context.last_modified.present?
          mime_typed_accepts = request.accept.map { |a| mime_type(a) }
          format = route.determine_format(mime_type(params[:format]), mime_typed_accepts)
          # If no format specified and is other than default
          # we must redirect to formatted route in order to avoid caching
          # from Varnish
          if params[:format].blank? && format != 'text/html'
            redirection = "#{route.id}#{Rack::Mime.mime_extension(format)}"
            redirect to(redirection)
          end
          # Force the content type header to the matched format
          content_type format
          status status_code
          renderer_for(route).output(context._locals, format)
        rescue RestClient::Exception => e
          # Rescue fetch context problems and...
          if e.http_code == 404
            # dispatch 404 error handling
            turn_404_into_search request.path_info
          else
            # Or throw a error
            halt 500
          end
        end
      end
    end

    # Return the first format that this route accepts from the ones requested by the client Accept header
    def determine_format(url_format, client_accepts)
      ([url_format] + client_accepts + ['text/html', formats.first]).detect do |format|
        formats.include? format
      end
    end

    protected

    def replace_placeholders_with(object); end
  end
end
