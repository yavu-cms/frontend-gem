module Yavu::API::Resource
  class NewsletterRoute < Route
    def controller
      route = self
      -> do
        begin
          cache_control *route.cache_control rescue nil
          content_type 'text/html'
          renderer_for(route).raw_output(fetch_context(route).newsletter)
        rescue
          turn_404_into_search request.path_info
        end
      end
    end

    def can_process?(object)
      object.is_a? NewsletterGeneratedData
    end

    protected

    def replace_placeholders_with(object)
      id.gsub(':newsletter', object.id)
    end
  end
end
