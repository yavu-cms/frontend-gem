module Yavu::API::Resource
  class SearchRoute < Route
    def can_process?(object)
      object.is_a? SearchQuery
    end

    protected

    def replace_placeholders_with(object)
      object.build_path(id)
    end
  end
end
