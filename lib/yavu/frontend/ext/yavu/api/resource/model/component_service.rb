module Yavu::API::Resource
  class ComponentService < Base
    def controller(route)
      service = self
      -> do
        cache_control *service.cache_control rescue nil
        renderer_for(route).render(partial: service.template, locals: fetch_service_context(service)._locals)
      end
    end
  end
end