module Yavu::API::Resource
  class SectionRoute < Route
    def can_process?(object)
      object.is_a? Section
    end

    protected

    def replace_placeholders_with(object)
      id.gsub(':section', object.id)
    end
  end
end
