module Yavu::API::Resource
  class ArticleRoute < Route
    def can_process?(object)
      object.is_a? Article
    end

    protected

    def replace_placeholders_with(object)
      id.gsub(':article', object.id).gsub(':section', object.section_id)
    end
  end
end