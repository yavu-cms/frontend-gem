require 'getoptlong'
require 'redis'
require 'redis-namespace'
require 'pidfile'

# This class is a CLI implementation that controls server frontend updates
# Its main responsability is to configure frontend and restart it when needed by backend
# All communication is done by redis queues
class Yavu::Frontend::Util::ServerMonitor
  cattr_accessor  :opts, :identifier, :namespace, :redis_url, :channel, :pid_file, :web_pid_file,
                  :signal, :token, :log_file, :started_at, :threshold, :heartbeat_namespace, :heartbeat_status_file
  @@_redis = nil
  @@logger = nil
  self.signal = 'SIGUSR2'
  self.started_at = Time.now.to_i
  self.threshold = 5

  self.opts = GetoptLong.new(
    [ '--help',           '-h',   GetoptLong::NO_ARGUMENT ],
    [ '--config',         '-C',   GetoptLong::OPTIONAL_ARGUMENT ],
    [ '--identifier',     '-i',   GetoptLong::REQUIRED_ARGUMENT ],
    [ '--namespace',      '-n',   GetoptLong::REQUIRED_ARGUMENT ],
    [ '--redis',          '-r',   GetoptLong::OPTIONAL_ARGUMENT ],
    [ '--channel',        '-c',   GetoptLong::REQUIRED_ARGUMENT ],
    [ '--pid-file',       '-p',   GetoptLong::REQUIRED_ARGUMENT ],
    [ '--web-server-pid', '-P',   GetoptLong::REQUIRED_ARGUMENT ],
    [ '--signal',         '-s',   GetoptLong::OPTIONAL_ARGUMENT ],
    [ '--token',          '-t',   GetoptLong::REQUIRED_ARGUMENT ],
    [ '--log-file',       '-l',   GetoptLong::OPTIONAL_ARGUMENT ])


  # This method provides a way to read configuration from config file
  def self.configure(&block)
    block.call(self)
    unless self.identifier.match(/^[\w\-.]+:\d+$/)
      puts "Identifier must be host:port but #{self.identifier} received"
      exit
    end
  end

  def self.process
    opts.each do |opt, arg|
      case opt
      when '--help'
        print_help
        exit 0
      when '--config'
        path = Pathname(arg)
        require(path.absolute? ? arg : File.join(Dir.pwd, arg))
      when '--identifier'
        self.identifier = arg
      when '--namespace'
        self.namespace = arg
      when '--redis'
        self.redis_url = arg
      when '--channel'
        self.channel = arg
      when '--pid-file'
        self.pid_file = arg
      when '--web-server-pid'
        self.web_pid_file = arg
      when '--signal'
        self.signal = arg unless arg.empty?
      when '--token'
        self.token = arg
      when '--log-file'
        self.log_file = File.open(arg, File::WRONLY | File::APPEND | File::CREAT)
        log_file.sync = true
      end
    end
    validate!
    PidFile.new(:piddir => File.dirname(pid_file), :pidfile => File.basename(pid_file))
    do_process
  end


  def self.logger
    unless @@logger
      self.log_file ||= STDOUT
      log = Logger.new(log_file)
      log.level = Logger.const_get((ENV['LOG_LEVEL'] || 'INFO').upcase)
      @@logger = log
    end
    @@logger
  end

  def self.heartbeat_status
    JSON.load(IO.read(heartbeat_status_file)) rescue nil
  end

  def self.token_string
    IO::read(token).chomp rescue nil
  end

  protected

  def self.redis
    unless @@_redis
      r = unless redis_url
            Redis.new
          else
            Redis.new url: redis_url
          end
      @@_redis = Redis::Namespace.new namespace, redis: r
    end
    @@_redis
  end

  def self.kill_server
    pid = Integer(IO.read(web_pid_file))
    logger.debug('Monitor') { "Sending signal=#{signal} pid=#{pid}" }
    Process.kill(signal, pid)
    self.started_at = Time.now.to_i
  rescue StandardError => e
    logger.error('Monitor') { "Error when trying to kill server error=#{e.message}" }
  end

  def self.do_process
    begin
      logger.info('Monitor') { "Starting up pid=#{$$}" }
      logger.debug('Monitor') { "Subscribing channel=#{channel} token=#{token_string}" }
      redis.subscribe(channel) do |on|
        on.message do |_, message|
          begin
            logger.debug('Monitor') { "Received message=#{message}" }
            hash = JSON.parse(message)
            if hash['identifier'] && hash['identifier'].chomp == token_string
              if (Time.now.to_i - self.started_at).abs < threshold
                raise "Server notified in less than #{threshold} seconds. Will not be notified"
              else
                if hash['hostname'] == identifier
                  kill_server
                end
              end
            elsif hash['id'] == identifier
              IO.write(token, hash['configure'])
              kill_server
              Process.kill(:HUP, $$)
            end
          rescue StandardError => e
            logger.error('Monitor') { "Error when processing message pid=#{$$} error=#{e.message}" }
          end
        end

      end
    rescue Redis::BaseConnectionError => error
      logger.error('Monitor') { "Error connecting to redis pid=#{$$} error=#{error} retry=1s" }
      sleep 1
      retry
    rescue SignalException => error
      if Signal.list['HUP'] == error.signo
        logger.warn('Monitor') { "Unexpected situation pid=#{$$} error=#{error}"}
        retry
      end
    end

  end

  def self.print_help
    puts <<-EOF
#{File.basename $0} [-h ] -n <namespace> [-r <redis-url>] -c <channel> -p <pid> [-s <signal>] -t <token>

  -h, --help: optional
  show help

  -C, --config <FILE>: optional
  Reads configuration parameters from FILE.
  Example configuration file:
    < CONF_FILE_SAMPLE >
      require 'socket' # So we get hostname
      Yavu::Frontend::Util::ServerMonitor.configure do |config|

        # Identification for this client. Commonly is hostname:port of frontend server
        # but can be any string that identifies this service so backend server can configure
        # us remotely·
        config.identifier = "#{Socket.gethostname}:#{ENV['PORT']}"

        # Heartbeat namespace to be used when beating so backend server knows our existence
        config.heartbeat_namespace = 'heartbeat-frontend-server'

        # Redis server. Must be shared with backend server.·
        # Must match backend_server/config/settings.yml
        #   redis:
        #     url: THIS VALUE
        #config.redis_url = 'redis://localhost:6379/0'

        # Redis namespace to use to communicate with backend server.·
        # Must match backend_server/config/settings.yml
        #   redis:
        #     namespace: THIS VALUE
        config.namespace = ENV['NAMESPACE'] || 'yavu-backend'

        # Channel used to talk with backend by redis
        # Muts match backend_server/conig/settings.yml
        #   redis:
        #     client_frontend_channel: THIS VALUE
        config.channel = ENV['CHANNEL'] || 'client-frontend-channel'


        # File where token is supposed to be read. Token is a string to watch if match to signal process
        # It is the Backend client_application model identifier that this frontend server serves
        config.token = File.expand_path 'client.id', File.join(__FILE__,'..')

        config.log_file = 'log/monitor.log'
      end
    < CONF_FILE_SAMPLE >

  -i, --identifier <ID>
  Set client identification. An identifier must be unique for backend, so using hostanme and port of
  this service is sufficient

  -n, --namespace <NAMESPACE>:
  Define redis namespace for messages

  -r, --redis <REDIS_URL>: optional
  Set Redis URL to use. If not specified assumes redis://localhost:6379/0

  -c, --channel <CHANNEL>
  Set channel to subscribe to

  -p, --pid-file <FILENAME>
  Pidfile of this process

  -P, --web-server-pid <FILENAME>
  Pidfile of web server to send signals to

  -t, --token <FILENAME>
  File where token is supposed to be read. Token is a string to watch if match to signal process
  It is the Backend client_application model identifier that this frontend server serves

  -l, --log-file <LOG>: optional
  Where to write logs
  EOF
  end

  def self.validate!
    required_values.each do |k,v|
      unless v
        puts "Missing #{k}. Try --help"
        exit 1
      end
    end
  end

  def self.required_values
    {namespace: namespace, channel: channel, pid: pid_file, token: token}
  end
end