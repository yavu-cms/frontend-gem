require 'sidekiq'
require 'sidekiq-cron'

class Yavu::Frontend::Util::ServerHeartbeat
  include Sidekiq::Worker

  def self.start
    Sidekiq::Cron::Job.create( name: 'Heartbeat every minute', cron: '*/1 * * * *', klass: 'Yavu::Frontend::Util::ServerHeartbeat')
  end

  def perform(*args)
    raise 'Must set Yavu::Frontend::Util::ServerMonitor.identifier' unless Yavu::Frontend::Util::ServerMonitor.identifier
    data = JSON.dump(ts: Time.now.to_i, identifier: Yavu::Frontend::Util::ServerMonitor.token_string, status: Yavu::Frontend::Util::ServerMonitor.heartbeat_status)
    Yavu::Frontend::Util::ServerMonitor.redis.hset Yavu::Frontend::Util::ServerMonitor.heartbeat_namespace, Yavu::Frontend::Util::ServerMonitor.identifier, data
    Sidekiq.logger.debug('Frontend') { "ServerHeartbeat sent data=#{data}"}
  end
end

