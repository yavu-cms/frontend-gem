require 'sass'
require 'uglifier'
require 'sprockets-helpers'

module Yavu
module Frontend
  module Util
  class AssetConfigurationFactory
    def self.sprockets_environment(assets_configuration)
      Sprockets::Environment.new(assets_configuration[:root]).tap do |environment|
        environment.js_compressor   = assets_configuration[:js_compressor].try :to_sym
        environment.css_compressor  = assets_configuration[:css_compressor].try :to_sym
        assets_configuration[:append_paths].each do |path|
          environment.append_path path
        end
        if gem = Gem::Specification.find_by_name('yavu-frontend')
          %w(javascripts stylesheets).each do |type|
            environment.append_path File.join(gem.full_gem_path,'lib','yavu','frontend','assets', type)
          end
        end
        environment.context_class.class_exec(assets_configuration[:base_url], 
          assets_configuration[:service_prefix]) do |captured_base_url, captured_service_prefix|
            cattr_accessor :base_url, :service_prefix

            self.base_url = captured_service_prefix
            self.service_prefix = captured_service_prefix

            def base_url
              self.class.base_url
            end

            def service_prefix
              self.class.service_prefix
            end
        end
      end
    end

    def self.sprockets_manifest(assets_configuration, environment)
      Sprockets::Manifest.new(environment, assets_configuration[:manifest_file])
    end

    def self.configure_sprockets_helpers(assets_configuration, environment, &block)
      if block_given?
        block.call environment.context_class
      end
      manifest = sprockets_manifest(assets_configuration, environment)
      Sprockets::Helpers.configure do |sprockets|
        sprockets.environment = environment
        sprockets.public_path = assets_configuration[:public_path]
        sprockets.prefix = assets_configuration[:prefix]
        sprockets.digest = assets_configuration[:digest]
        sprockets.debug = assets_configuration[:debug]
        sprockets.asset_host = assets_configuration[:asset_host]
        sprockets.manifest = manifest
      end
      manifest
    end

  end
  end
end
end
