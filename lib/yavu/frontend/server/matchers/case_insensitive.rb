module Yavu
  module Frontend
    module Server
      class CaseInsensitiveMatcher
        attr_reader :keys

        def initialize(base)
          @regexp, @keys = Sinatra::Base.send(:compile, base)
          @regexp = /#{@regexp}/i # Ensure regexp is case-insensitive
        end

        # Returns a MatchData object or nil if there is no match
        def match(str)
          @regexp.match(ensure_format_dot(str.downcase))
        end

        private

        def ensure_format_dot(str)
          !str.include?('.') ? (str + '.') : str
        end
      end
    end
  end
end
