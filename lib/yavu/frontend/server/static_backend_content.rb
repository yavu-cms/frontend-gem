require 'logger'
module Yavu
module Frontend
module Server
  class StaticBackendContent < Sinatra::Base
    set :logger,               Logger.new(STDOUT)
    set :api_base,             'http://localhost:3000'
    set :client_configuration, nil
    enable :static_backend_content

    get('/favicon.ico') { 200 }

    def self.configure_client!
      %W(#{media_url}/* #{assets_url}/*).each do |url|
        logger.debug('Frontend') { "New StaticBackendContent endpoint pattern=#{url}" }
        get url do
          response = api_client[request.path].get proxy_headers
          content_type response.headers[:content_type]
          cache_control response.headers[:cache_control] if response.headers[:cache_control]
          last_modified response.headers[:last_modified] if response.headers[:last_modified]
          response.to_s
        end
      end if static_backend_content
    end

    def self.media_url
      raise ArgumentError, 'internal_media_url must be set in client_configuration' unless client_configuration.internal_media_url
      client_configuration.internal_media_url
    end

    def self.assets_url
      raise ArgumentError, 'prefix must be set in client_configuration.assets_configuration' unless client_configuration.assets_configuration.prefix
      client_configuration.assets_configuration.prefix
    end

    def proxy_headers
      { 'X-Forwarded-For' => (request.env['X-Forwarded-For'].to_s.split(/, +/) + [request.env['REMOTE_ADDR']]).join(', '),
        'Accept-Encoding' => request.accept_encoding,
        'Referer'         => request.referer,
        'Cache-control'   => 'no-cache' }
    end

    register Yavu::Frontend::Server::APIExtension
  end
end
end
end