module Yavu
module Frontend
  module Server
  module APIExtension
    module Helpers
      def api_client; self.class.api_client end
      def fetch(uri, method: :get, params: {}, headers: {}, halt_on_exception: false)
        self.class.fetch uri, params: params, headers: headers
      rescue RestClient::Exception => e
        if halt_on_exception
          halt e.http_code
        else
          raise e
        end
      end
    end

    def api_client
      RestClient::Resource.new settings.api_base
    end

    # Get a resource using the API client
    def fetch(uri, method: :get, params: {}, headers: {})
      uri.gsub! %r{^/api/}, '/'
      settings.logger.info('Frontend') { "Performing API request method=#{method.upcase} uri=#{uri} params=#{params.inspect} headers=#{headers}" }
      json = api_client[uri].send(method, headers.merge(params: params, accept: :json))
      ::Yavu::API::Resource::Base.from_json(json).tap do |resource|
        # Propagate the ETag + Last-Modified information provided by the API whenever possible
        begin
          # It's necessary to remove the original quotes so that the final ETag doesn't duplicate them
          resource.etag = json.headers[:etag].gsub(/\A"|"\z/, '') if json.headers[:etag].present?
          resource.last_modified = json.headers[:last_modified] if json.headers[:last_modified].present?
        rescue NoMethodError
        end
      end
    end

    def self.registered(app)
      app.helpers Helpers
    end
  end
  end

end
end
