require 'sinatra/base'
require 'rest_client'

require 'yavu/frontend/server/extensions/api_extension'
require 'yavu/frontend/server/static_backend_content'
require 'yavu/frontend/server/matchers/case_insensitive'
require 'yavu/api/resource'
require 'action_dispatch'
require 'uri'

module Yavu
  module Frontend
    module Server
      class Base < Sinatra::Base
        set :logger,                       Logger.new(STDOUT)
        set :api_base,                     'http://localhost:3000'
        set :client_id,                    ''
        set :client_routes,                Hash.new
        set :renderer,                     Yavu::Frontend::Renderer::Base
        set :locale,                       'en'
        set :session_secret,               -> { "yavu-frontend-#{settings.client_id}" }
        set :session_key,                  '_yavu_session'
        set :session_domain,               :all
        set :signed_cookie_salt,           'signed cookie'
        set :encrypted_cookie_salt,        'encrypted cookie'
        set :encrypted_signed_cookie_salt, 'signed encrypted cookie'
        set :root_path,                    '/'
        set :runtime_options,              Hash.new
        set :manifest_path,                nil
        set :ill_status,                   503
        set :healthy_check_endpoint,       '/_yavu_frontend_status'

        disable :static
        enable :static_backend_content

        if ENV['RACK_ENV'] == 'development'
          disable :raise_errors
          disable :show_exceptions
        end

        # Override this to provide a better misconfiguration error handling
        error Yavu::Frontend::Server::MisconfiguredServerError do
          puts 'Misconfiguration problem!'
        end

        configure do
          # Load translations
          I18n.load_path = Dir[File.join settings.root, 'config', 'locale', '*.yml']
          I18n.reload!
        end

        # This hook should be called when a fatal error ocurrs in configuration, for example
        def self.illness
          ill = ->{ halt settings.ill_status }
          %i( get head post put patch delete ).each { |m| send(m, '*', &ill) }
        end

        def self.configure_client!
          set :client_configuration, fetch('api/register', params: {id: client_id})
          set :root_path, client_configuration.base_url
          set :runtime_options, client_configuration.runtime_options
          configure_renderer(client_configuration)
          configure_sprockets(client_configuration.assets_configuration)
          mount_middlewares
          mount_protection
          mount_healthy_check_route
          mount_client_routes_and_services
          mount_wildcard_routes
        end

        def self.configure_renderer(configuration)
          renderer.configure do |config|
            config.base_url       = configuration.base_url
            config.service_prefix = configuration.service_prefix
            config.media_url      = (configuration.external_media_url || '').chomp('/')
            config.routes         = configuration.routes
            config.logo           = configuration.logo
            config.assets_configuration = configuration.assets_configuration
            config.runtime_options = configuration.runtime_options
          end
        end

        def self.configure_sprockets(assets_configuration)
          raise 'Manifest path is not set, please set :manifest_path in your sinatra app' unless manifest_path
          begin
            content = fetch("#{assets_configuration['prefix']}/manifest.json")
            IO.write(manifest_path, JSON.dump(content))
            assets_configuration['manifest_file'] = manifest_path
            renderer.configure_sprockets
            logger.debug('Frontend') { "Sprockets configuration=#{assets_configuration.inspect}" }
          rescue RestClient::Exception => e
            logger.error('Frontend') { "Error while configuring sprockets: #{e.message}" }
          end
        end

        def self.middlewares(&block)
          @@_middlewares = block
        end

        def self.mount_middlewares
          mount_session_middlewares
          begin
            @@_middlewares.call if @@_middlewares.respond_to?(:call)
          rescue NameError
          end
          StaticBackendContent.configure do |config|
            config.api_base               = api_base
            config.logger                 = logger
            config.client_configuration   = client_configuration
            config.static_backend_content = static_backend_content
          end
          StaticBackendContent.configure_client!
          use StaticBackendContent
        end

        def self.mount_session_middlewares
          define_singleton_method :key_generator do
            @caching_key_generator ||= ActiveSupport::CachingKeyGenerator.new(
              ActiveSupport::KeyGenerator.new(settings.session_secret, iterations: 1000)
            )
          end

          use Rack::Config do |env|
            env['action_dispatch.key_generator'] = key_generator
            env['action_dispatch.secret_key_base'] = session_secret
            env['action_dispatch.signed_cookie_salt'] = signed_cookie_salt
            env['action_dispatch.encrypted_cookie_salt'] = encrypted_cookie_salt
            env['action_dispatch.encrypted_signed_cookie_salt'] = encrypted_signed_cookie_salt
          end

          use ActionDispatch::Session::CookieStore, key: session_key, secret: session_secret, domain: session_domain
        end

        def self.mount_protection
          use Rack::Protection, session: true
        end

        def self.mount_client_routes_and_services
          client_configuration.routes.sort { |x, y| x.priority <=> y.priority }.reverse_each do |route|
            client_routes[route.id] = route
            send "mount_#{route.id == '/' ? 'home' : 'route'}", route
            mount_services route if route.services
          end
        end

        def self.mount_home(route)
          body = proc do
            request.session_options[:skip] = true unless route.cache_control.try(:match, /private/)
            instance_exec(&route.controller)
          end
          send(route.http_method, '/', &body)
          send(route.http_method, '/.:format', process_route_options(route), &body) if route.formats.present?
        end

        def self.ci(pattern)
          CaseInsensitiveMatcher.new(pattern)
        end

        def self.mount_route(route)
          logger.debug('Frontend') { "New route pattern=#{route.id} method=#{route.http_method} format=#{format_suffix}" }
          send(route.http_method, ci(route.id + format_suffix), process_route_options(route)) do
            request.session_options[:skip] = true unless route.cache_control.try(:match, /private/)
            instance_exec(&route.controller)
          end
        end

        def self.mount_services(route)
          route.services.each do |service|
            r = renderer.new(route, logger: logger)
            service_pattern = r.service_path service
            logger.debug('Frontend') { "New service id=#{service.id}:#{service.name} method=#{service.http_method} pattern=#{service_pattern}" }
            send(service.http_method.downcase, service_pattern) do
              request.session_options[:skip] = true unless service.cache_control.match(/private/)
              instance_exec(&service.controller(route))
            end
          end
        end

        def self.format_suffix
          '.:format?'
        end

        def self.process_route_options(route)
          route.formats.present?? {provides: route.formats} : {}
        end

        def self.mount_healthy_check_route
          get settings.healthy_check_endpoint do
            # Returns http status 200
          end
        end

        # Adds wildcard routes to handle 404s:
        #   - A wildcard route for catching unknown service calls and returning actual 404s
        #   - A wildcard route for catching regular 404s and redirecting them to the search route
        def self.mount_wildcard_routes
          logger.debug('Frontend') { "Service wildcard route pattern=#{client_configuration.service_prefix}/*" }
          get "#{client_configuration.service_prefix}/*" do
            halt 404
          end

          logger.debug('Frontend') { "General wildcard route pattern=/*" }
          get '/*' do
            halt 404 if settings.client_routes.empty? || request.path_info =~ /\.\w{0,4}$/
            turn_404_into_search(request.path_info)
          end
        end

        def authenticated_user; end # Override this method to implement a user session

        def renderer_for(route)
          settings.renderer.new settings.client_routes[route.id], locale: settings.locale, logger: settings.logger, server: self
        end

        def fetch_context(route)
          fetch("api/context/#{settings.client_id}", params: {route: route.id, params: context_params.to_json}, headers: context_headers, halt_on_exception: false)
        end

        def fetch_service_context(service)
          fetch("api/service/#{settings.client_id}/#{service.component_configuration_id}/#{service.id}", method: service.http_method, params: context_params, headers: service_context_headers, halt_on_exception: true)
        end

        def context_params
          params.delete_if { |k, v| %w(splat captures).include? k }
        end

        def context_headers
          {'X-YAVU-USER' => authenticated_user ? Yavu::API::Resource::User.new(authenticated_user.to_hash).to_json : MultiJson.dump(nil) }
        end
        alias :service_context_headers :context_headers

        # Retrieve user IP from HTTP Headers (to bypass reverse proxies) or from default request
        def request_ip
          request.ip
        end

        def turn_404_into_search(query)
          halt 404 unless (route = settings.client_routes.detect { |_, v| v.is_a? Yavu::API::Resource::SearchRoute }.try(:last))
          params[:query] = URI::unescape(query.to_s).parameterize.underscore.humanize.downcase
          instance_exec(&route.controller(status_code: 404))
        rescue
          halt 404
        end

        register Yavu::Frontend::Server::APIExtension
      end
    end
  end
end
