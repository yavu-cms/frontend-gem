module Yavu
  module Frontend
    module Server
      class MisconfiguredServerError < ::RuntimeError
      end
    end
  end
end
